package main

import (
	"fmt"
	"sync"
	"time"
)

var wg sync.WaitGroup
var lineas []int
var lin []int

func imprimeSlice(lin []int, invocacion int) {
	defer wg.Done()
	for i := 0; i < len(lin); i++ {
		//fmt.Printf("Invocación %d - línea %d\n", invocacion, lin[i])
		fmt.Printf("%d;%d\n", invocacion, lin[i])
	}
}

func ObtenerFechaActual() string {
	t := time.Now()
	fecha := fmt.Sprintf("%d-%02d-%02d %02d:%02d:%02d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())

	return fecha
}

func main() {

	fecha := ObtenerFechaActual()

	corte := 25

	capacidad := 1000026

	//lineas := make([]int, 0, capacidad)

	fmt.Println("Prueba de Concurrencia con Slice")

	for i := 0; i < capacidad; i++ {
		lineas = append(lineas, i)
	}

	cantLineas := len(lineas)

	cantProcesos := cantLineas / corte

	cantResto := cantLineas % corte

	if cantResto > 0 {
		cantProcesos = cantProcesos + 1
	}

	wg.Add(cantProcesos)

	for i := 0; i < cantProcesos; i++ {

		switch i {
		case 0:
			lin = lineas[0:corte]
		case cantProcesos - 1:
			lin = lineas[corte*i : len(lineas)]
		default:
			lin = lineas[corte*i : corte*(i+1)]
		}

		go imprimeSlice(lin, i+1)
	}

	wg.Wait()

	fmt.Printf("Inicia Proceso %s\n", fecha)

	fmt.Println("Cantidad de líneas a procesadas:", cantLineas)

	fmt.Printf("Procesos realizados: %d\n", cantProcesos)

	fecha = ObtenerFechaActual()

	fmt.Printf("Finaliza proceso %s\n", fecha)
}
