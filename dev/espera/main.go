package main

import (
	"fmt"
	"sync"
)

var (
	contador = 0
//	lock     sync.Mutex
)

func main() {
	wg := new(sync.WaitGroup)
	wg.Add(3)
	for i := 0; i < 3; i++ {
		go incremento(wg)
	}
	wg.Wait()
	fmt.Println("Finaliza")
}

func incremento(wg *sync.WaitGroup) {
//	lock.Lock()
//	defer lock.Unlock()
	contador++
	fmt.Println("Cuenta:", contador)
	wg.Done()
}
