package main

import ("fmt"
"time")

func main(){
	fmt.Println("Inicio")
	/* Invocación de la goroutine*/
	go proceso()
	time.Sleep(time.Millisecond * 10) /* sólo a los efectos de visualizar la ejecución de la goroutine - no recomendable*/
	fmt.Println("Finaliza")
}

func proceso(){
	fmt.Println("Procesando....")
}