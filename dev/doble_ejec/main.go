package main

import (
	"fmt"
	"time"
)

var contador = 0

func main() {
	for i := 0; i < 3; i++ {
		go incremento()
	}
	time.Sleep(time.Millisecond * 10) /* sólo a los efectos de visualizar la ejecución de la goroutine - no recomendable*/
	fmt.Println("Finaliza")
}

func incremento() {
	contador++
	fmt.Println("Cuenta:", contador)
}
